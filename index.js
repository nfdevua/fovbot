const axios = require('axios');
let cheerio = require('cheerio');
const { Telegraf } = require('telegraf')
const bot = new Telegraf('1489065942:AAEyF6WcKl4Pvmq5A9fmsF8QhS6VAbb3g0o')


bot.command('rates', (ctx) => {
  
  let sendText = ``;


  const requestOne = axios.get('https://coinmarketcap.com/')
  const requestTwo = axios.get('https://ru.investing.com/currencies/usd-rub-chart')

  axios.all([requestOne, requestTwo])
    .then(axios.spread((...responses) => {

      const html2 = responses[1].data;
      const $2 = cheerio.load(html2); 
      const convertedValue = $2('#last_last').text();

      const html1 = responses[0].data;
      const $ = cheerio.load(html1); 

      $('table.cmc-table > tbody > tr').slice(0,10).each((index, element) => {
        const title = $(element).find('td:nth-child(3) > a > div > div > div > p').text();
        const price = $(element).find('td:nth-child(4)').text().substring(1);
        const percentStatus = ($(element).find('td:nth-child(5) > span > span').hasClass('icon-Caret-down')) ? '-' : '+';
        const percent = $(element).find('td:nth-child(5)').text();
        
        let convertedPrice = price.replace(',','')*convertedValue.replace(',','.');
        convertedPrice = (convertedPrice.toFixed(2)+'').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
        
        sendText += ` ${index+1} | ${title} | 🇺🇸 ${price.replace(',',' ')} $ | 🇷🇺 ${convertedPrice} ₽ 📈 ${percentStatus}${percent} \n`
      })

      ctx.reply(sendText)
    }));
})
bot.launch()



